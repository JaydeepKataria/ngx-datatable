import { SMSCardPage } from './app.po';

describe('smscard App', () => {
  let page: SMSCardPage;

  beforeEach(() => {
    page = new SMSCardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
