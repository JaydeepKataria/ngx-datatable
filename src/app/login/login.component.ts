import { Router } from '@angular/router';
import { AdminService } from '../common/admin.service';
import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: any = {};

  constructor(
    private adminService: AdminService,
    private cookieService: CookieService,
    private flashMessagesService: FlashMessagesService,
    private router: Router) { }

  ngOnInit() {
    if ((this.cookieService.get('sEmail') != '') && (this.cookieService.get('sPassword') != '')) {
      this.loginData.sEmail = this.cookieService.get('sEmail');
      this.loginData.sPassword = this.cookieService.get('sPassword');
      this.loginData.bRememberMe = true;
    }
  }

  login() {
    this.adminService.login(this.loginData)
      .subscribe(
      res => {
        if (res) {
          localStorage.setItem('token', res['token']);
          if (this.loginData.bRememberMe == true) {
            this.cookieService.set('sEmail', this.loginData.sEmail);
            this.cookieService.set('sPassword', this.loginData.sPassword);
          } else {
            this.cookieService.deleteAll();
          }
          this.router.navigate(['/admin/dashboard']);
        }
      },
      error => {
        if (error.status == 401) {
          this.flashMessagesService.show('Please check User name and Password', { cssClass: 'alert-danger', timeout: 2000 })
          this.router.navigate(['/']);
        }
        else if (error.status == 403) {
          this.flashMessagesService.show('User does not exists', { cssClass: 'alert-danger', timeout: 2000 })
          this.router.navigate(['/']);
        }
        else {
          this.flashMessagesService.show(error.error.message, { cssClass: 'alert-danger', timeout: 2000 })
          this.router.navigate(['/']);
        }

      }
      );

  }

}
