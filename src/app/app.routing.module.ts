import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './common/auth.guard';
import { LoginComponent } from "app/login/login.component";
import { ForgotPasswordComponent } from 'app/forgot-password/forgot-password.component';

const routes = [
    { path: '', component: LoginComponent, canActivate: [AuthGuard]},
    { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule', canActivate: [AuthGuard]},
    { path: 'forgot-password', component: ForgotPasswordComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [LoginComponent,ForgotPasswordComponent]