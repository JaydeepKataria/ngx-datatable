import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from 'app/app.routing.module';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthGuard } from './common/auth.guard';
import { AuthService } from './common/auth.service';
import { AdminService } from './common/admin.service';
import { CookieService } from 'ngx-cookie-service';
import { InterceptorService } from './common/interceptor.service';
import 'hammerjs';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AdminModule } from 'app/admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPasswordComponent,
 ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    FlashMessagesModule,
    MaterialModule,
    BrowserAnimationsModule,
    AdminModule,
  
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
      
    },
    CookieService,
    AdminService,
    AuthGuard,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
