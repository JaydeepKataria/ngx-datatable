import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AdminService {
  data: any;
  private baseUrl: string = environment.apiUrl + environment.version;
  private subject = new Subject<any>();
  constructor(
    private http: HttpClient
  ) { }
  private getHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return headers;
  }
  private formData(myFormData) {
    return Object.keys(myFormData).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(myFormData[key]);
    }).join('&');
  }

//   private getAuthToken() {
//     var storageValues = localStorage.getItem('ARTOKEN');
//     if (storageValues != null) {
//       return 'bearer ' + storageValues;
//     } else {
//       return '';
//     }
//   }
  
 login(data) {
    return this.http.post(`${this.baseUrl}/admin/adminLogin`, data);
  }
  
  changepass(data) {
    return this.http.post(`${this.baseUrl}/admin/changepass`, data);
  }

  forgotPassword(email) {
    return this.http.post(`${this.baseUrl}/admin/forgotpass`, email);
  }

  resetPassword(user) {
    return this.http.post(`${this.baseUrl}/admin/resetpass`, user);
  }

  getDashboardInfo() {
      var headers = this.getHeaders();
      return this.http.post(`${this.baseUrl}/admin/getdasboardInfo`, { headers: headers })
        .map((response: Response) => {
          return response;
        });  
  }

  getSingleUser(id) {
    let params = new HttpParams();
    params = params.append('id', id);
    console.log(id);
    return this.http.get(`${this.baseUrl}/user/user`, { params: params });
  }

  statusUser(id, eStatus) {
    let params = new HttpParams();
    params = params.append('id', id);
    params = params.append('status', eStatus);
      var headers = this.getHeaders();
      return this.http.get(`${this.baseUrl}/user/userStatus?${params}`).map((response: Response) => {
        return response;
      });
  }

  getUsers(data) {
    return this.http.post(`${this.baseUrl}/user/listUser`, data);
  }

  updateUser(data) {
    return this.http.post(`${this.baseUrl}/admin/user/update`, data);
  }

  addSingleUser(data) {
    console.log(data);
    return this.http.post(`${this.baseUrl}/admin/user`, data)
      .map((response: Response) => {
        return response;
      });
  }

  getSiteSettings() {
    return this.http.post(`${this.baseUrl}/general_setting/getSiteSettings`, {})
      .map((response: Response) => {
        return response;
      });
  }

  updateSiteSettings(data) {
    return this.http.post(`${this.baseUrl}/general_setting/updateSiteSettings`, data)
      .map((response: Response) => {
        this.data = response;
        this.subject.next({ sLogoUrl: this.data.doc.sLogoUrl });
        return response;
      });
  }

  getLogo(): Observable<any> {
    return this.subject.asObservable();
  }

}
