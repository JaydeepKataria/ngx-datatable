import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/do';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (localStorage.getItem('token')) {
      const authHeader = 'bearer ' + localStorage.getItem('token');
      const authReq = req.clone({headers: req.headers.set('Authorization', authHeader)});
      return next.handle(authReq);
    }
    return next.handle(req).do((event) => {
      if (event instanceof HttpResponse) {
         // detect reponse
         // console.log('response ', event);
      }
    }, (err: any) => {
      if (err['url'] !== environment.apiUrl + environment.version + '/admin/authenticate') {
        if ( err['status'] === 401)  {
          this.router.navigate(['/']);
        }
      }
    });
  }
}
