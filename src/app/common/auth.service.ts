import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
    constructor(private router: Router) { }

    getToken(): string {
        return localStorage.getItem('token');
    }
    isAuthenticate(url: string): boolean {
        // get the token
        const token = this.getToken();
        // return a boolean reflecting 
        // whether or not the token is expired
        if (token !== null && token !== '') {
            if (url === '/') {
                this.router.navigate(['/admin/dashboard']);
                return false;
            } else {
                return true;
            }
        } else {
            if (url !== '/') {
                this.router.navigate(['/']);
                return false;
            } else {
                return true;
            }
        }
    }
}