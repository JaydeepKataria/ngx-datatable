import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-site-setting',
  templateUrl: './site-setting.component.html',
  styleUrls: ['./site-setting.component.scss']
})
export class SiteSettingComponent implements OnInit {
  public settingsForm: FormGroup;
  constructor(private fb: FormBuilder, ) { }

  ngOnInit() {
    this.settingsForm = this.fb.group({
      sSiteName: ['', Validators.compose([Validators.required])],
      sDescription: ['', Validators.compose([Validators.required])],
      dCreatedDate: [null, Validators.compose([Validators.required])],
      dUpdatedDate: [null, Validators.compose([Validators.required])],
      sUpdatedby: ['', Validators.compose([Validators.required])],
     
    });

  }

}
