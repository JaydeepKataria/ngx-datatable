import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, MdNativeDateModule, MdDatepickerModule } from '@angular/material';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin.component'
import { AdminRoutingModule } from 'app/admin/admin-routing.module';
import { ChangePasswordComponent } from 'app/admin/change-password/change-password.component';
import { DetailsDialogComponent } from './common/details-dialog/details-dialog.component';
import { MenuToggleModule } from 'app/common/menu/menu-toggle.module';
import { UserModule } from 'app/admin/user/user.module';
import { ConfirmDialogComponent } from './common/confirm-dialog/confirm-dialog.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { SiteSettingComponent } from 'app/admin/site-setting/site-setting.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DashboardComponent,
    AdminComponent,
    ChangePasswordComponent,
    ConfirmDialogComponent,
    DetailsDialogComponent,
    SiteSettingComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    MenuToggleModule,
    UserModule,
    ReactiveFormsModule,
    FlashMessagesModule,
    MdDatepickerModule,
    MdNativeDateModule
  ],
  entryComponents: [ConfirmDialogComponent,DetailsDialogComponent],
})
export class AdminModule { }
