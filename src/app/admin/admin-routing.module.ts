import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from 'app/admin/dashboard/dashboard.component';
import { AdminComponent } from 'app/admin/admin.component';
import { Route } from '@angular/router/src/config';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SiteSettingComponent } from 'app/admin/site-setting/site-setting.component';


const routes = [
    {
        path: '', component: AdminComponent,
        children: [
            {
                path: 'dashboard', component: DashboardComponent
            },
            {
                path: 'change-password', component: ChangePasswordComponent
            },
            {
                path: 'user' , loadChildren: 'app/admin/user/user.module#UserModule' 
            },
            {
                path: 'site-setting' , component: SiteSettingComponent 
            }
        ]
    },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }