import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from 'app/admin/user/form/form.component';
import { ListComponent } from 'app/admin/user/list/list.component';
import { MaterialModule } from '@angular/material';
import { UserRoutingModule } from 'app/admin/user/user-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FlashMessagesModule } from 'angular2-flash-messages';
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    UserRoutingModule,
    NgxDatatableModule,
    FlashMessagesModule
  ],
  declarations: [
    FormComponent,
    ListComponent
  ]
})
export class UserModule { }
