import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AdminService } from '../../../common/admin.service';
import { element } from 'protractor';
import { Page } from "./page";
import { MdDialog, MdDialogRef } from '@angular/material';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ConfirmDialogComponent } from '../../common/confirm-dialog/confirm-dialog.component';
import { DetailsDialogComponent } from './../../common/details-dialog/details-dialog.component';
import { Router } from '@angular/router';
@Component({
  selector: 'basic-bootstrap-theme-demo',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  status: any;
  userData: any;

  page = new Page();
  temp2: any; // this the new temp array
  pagedData: any;
  rows = [];
  users: any;
  temp = [];
  userOne: any;
  columns = [];
  eStatus: any = [];
  loading: boolean = false;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('editTmpl') editTmpl: TemplateRef<any>;
  @ViewChild('hdrTmpl') hdrTmpl: TemplateRef<any>;
  confirmDialog: MdDialogRef<ConfirmDialogComponent>;
  detailsDialog: MdDialogRef<DetailsDialogComponent>;
  constructor(
    private adminService: AdminService,
    private flashMessagesService: FlashMessagesService,
    private dialog: MdDialog,
    private router: Router
  ) {
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.page.sorts = { dir: "asc", prop: "Firstname" }
    this.page.search = '';
  }

  ngOnInit() {
    this.setPage({ offset: 0 });
    this.columns = [
      { prop: 'Firstname' },
      { prop: 'Lastname' },
      { prop: 'Email' },
      { prop: 'Status', cellTemplate: this.editTmpl, sortable: false },
      { prop: 'Operation', cellTemplate: this.hdrTmpl, sortable: false }
    ]


  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    //console.log(this.page);
    this.adminService.getUsers(this.page).subscribe(data => {
      this.pagedData = data;
      this.page.totalElements = this.pagedData.count[0].totalUser;
      this.rows = [];
      this.userData = this.pagedData.data;
      this.userData.forEach(element => {
        this.userOne = element;
        this.userOne.eStatus === 'y' ? this.userOne.eStatus = 'checked' : this.userOne.eStatus = '';
        //this.eStatus.push(this.userOne.eStatus);
        this.rows.push({ Firstname: this.userOne.sFirstName, Lastname: this.userOne.sLastName, Email: this.userOne.sEmail });
        this.temp = [...this.rows];
      })
    }, err => {
      console.log(err);
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    console.log(val);
    this.page.search = val;
    this.setPage({ offset: this.page.pageNumber });
  }

  onSort(event) {
    this.loading = true;
    setTimeout(() => {
      const rows = [...this.rows];
      this.page.sorts = event.sorts[0];
      this.page.sorts.prop == 'Firstname' ? 'sFirstName' : 'sEmail';
      this.page.sorts.prop == 'Lastname' ? 'sLastName' : 'sEmail';
      this.setPage({ offset: this.page.pageNumber });
      this.loading = false;
    }, 1000);
  }

  statusChange($e, id) {
    var status = $e.target.checked;
    status === true ? status = 'y' : status = 'n';
    this.adminService.statusUser(id, status).subscribe((response) => {
      if (response) {
        console.log(response)
        this.flashMessagesService.show(response['temp'], { cssClass: 'alert-success', timeout: 1000 });
      }
    }, error => {
      console.log(error);
      this.flashMessagesService.show(error.error.message, { cssClass: 'alert-danger', timeout: 1000 })
    });
  }

  detailUser(id) {
    console.log(id);
    this.detailsDialog = this.dialog.open(DetailsDialogComponent, {
      disableClose: false
    });
    this.adminService.getSingleUser(id).subscribe(response => {
      this.detailsDialog.componentInstance.dialogTitle = 'User details';
      this.detailsDialog.componentInstance.dialogDetails = response['data'];
    }, err => {
      console.log(err);
    });
  }

  deleteUser(id, index) {
    var status = 'd';
    this.confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      disableClose: false
    });
    this.confirmDialog.componentInstance.confirmMessage = 'Are you sure, you want to delete this user ?';
    this.confirmDialog.afterClosed().subscribe(result => {
      if (result) {
        this.adminService.statusUser(id, status).subscribe((data) => {
          if (data) {
            this.flashMessagesService.show('user succesfully deleted', { cssClass: 'alert-success', timeout: 1000 });
            this.rows = [...this.temp];
            console.log(index);
            const temp = this.temp.splice(index, 1);
            this.rows = this.temp
          }
        }, error => {
          this.flashMessagesService.show(error.error.message, { cssClass: 'alert-danger', timeout: 1000 })
        }
        );
      }
    })
  }

  editUser(id) {
    this.router.navigate(['/admin/user/form']);
  }

  updateFilterOffline(event) {
    const val = event.target.value.toLowerCase();
    // and here you have to initialize it with your data
    this.rows = [...this.temp]; // and here you have to initialize it with your data
    // filter our data
    const temp = this.rows.filter(function (data) {
      const string = data.Firstname + data.Email + data.Status
      return string.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onSortOffline(event) {
    // event was triggered, start sort sequence
    console.log('Sort Event', event);
    this.loading = true;
    // emulate a server request with a timeout
    setTimeout(() => {
      const rows = [...this.rows];
      // this is only for demo purposes, normally
      // your server would return the result for
      // you and you would just set the rows prop
      const sort = event.sorts[0];
      rows.sort((a, b) => {
        return a[sort.prop].localeCompare(b[sort.prop]) * (sort.dir === 'desc' ? -1 : 1);
      });

      this.rows = rows;
      this.loading = false;
    }, 1000);
  }
}

