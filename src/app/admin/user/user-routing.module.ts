import { FormComponent } from 'app/admin/user/form/form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from 'app/admin/user/list/list.component';

const routes: Routes = [

    {
        path: 'form', component: FormComponent
    },
    {
        path: 'list', component: ListComponent
    },
    {
        path: 'add', component: FormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
