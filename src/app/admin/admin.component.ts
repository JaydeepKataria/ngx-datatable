import { Component, OnInit } from '@angular/core';
import { Router,NavigationStart ,NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  navigation: boolean;

  constructor(private router: Router) { 
    router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.navigation = true;
      }
      if (e instanceof NavigationEnd) {
        this.navigation = false;
      }
    });
  }

  ngOnInit() {
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
