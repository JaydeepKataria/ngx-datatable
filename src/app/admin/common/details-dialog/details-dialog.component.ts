import { MdDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details-dialog',
  templateUrl: './details-dialog.component.html',
  styleUrls: ['./details-dialog.component.scss']
})
export class DetailsDialogComponent implements OnInit {
  public dialogTitle: string;
  public dialogDetails: any;
  constructor(public detailDialog: MdDialogRef<DetailsDialogComponent>) {
  }
  ngOnInit() {
  }
  closeDialog() {
    this.detailDialog.close();
  }
}
